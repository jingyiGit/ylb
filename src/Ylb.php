<?php

namespace Ylb;

use Ylb\Kernel\Http;

class Ylb
{
  private $site = 'https://k.ylb.io';
  private $apikey_sms = '';
  private $apikey_voice = '';
  
  public function __construct($apikey_sms, $apikey_voice)
  {
    $this->apikey_sms   = $apikey_sms;
    $this->apikey_voice = $apikey_voice;
  }
  
  /**
   * 取帐户余额
   *
   * @return mixed
   */
  public function getBalance()
  {
    return $this->requestGet('/api/account/v0/balance', 'sms');
  }
  
  /**
   * 取帐单列表
   *
   * @param string $dayfrom 格式 yyyymmdd 默认为上个月的1号开始
   * @param string $dayto   格式 yyyymmdd 默认为昨天, 一次请求时间跨度最大100天
   * @return void
   */
  public function getStatement($dayfrom = '', $dayto = '')
  {
    return $this->requestGet('/api/account/v0/statement', 'sms', [
      'dayfrom' => $dayfrom,
      'dayto'   => $dayto,
    ]);
  }
  
  /**
   * 发送短信
   *
   * @param array $params
   * @return mixed
   */
  public function sendSms($params)
  {
    return $this->requestPost('/api/message/v0/send', 'sms', $params);
  }
  
  /**
   * 取发送短信的报告列表(每个发送记录只会被获取1次，获取后将清除)
   *
   * @return mixed
   */
  public function reportSms()
  {
    return $this->requestGet('/api/message/v0/report', 'sms');
  }
  
  /**
   * 发送语音通知
   *
   * @param array $params
   * @return mixed
   */
  public function sendVoice($params)
  {
    if (!isset($params['pid'])) {
      $params['pid'] = 1;
    }
    return $this->requestPost('/api/ivr/v0/send', 'voice', $params);
  }
  
  /**
   * 取语音报告的报告列表(每个发送记录只会被获取1次，获取后将清除)
   *
   * @return mixed
   */
  public function reportVoice()
  {
    return $this->requestGet('/api/ivr/v0/report', 'voice');
  }
  
  private function requestGet($url, $type = 'sms', $params = [])
  {
    $data = array_merge([
      'ts'     => time(),
      'apikey' => $type == 'sms' ? $this->apikey_sms : $this->apikey_voice,
    ], $params);
    return Http::httpGet($this->site . $url . '?' . http_build_query($data));
  }
  
  private function requestPost($url, $type = 'sms', $params = [])
  {
    $urlParam = [
      'ts'     => time(),
      'apikey' => $type == 'sms' ? $this->apikey_sms : $this->apikey_voice,
    ];
    return Http::httpPost($this->site . $url . '?' . http_build_query($urlParam), $params);
  }
}
